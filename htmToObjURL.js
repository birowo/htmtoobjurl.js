function htmToObjURL(htm){
  var buf = Uint8Array.from(htm.split(''), x=>x.charCodeAt(0)).buffer
	var blob = new Blob([buf], {type: 'text/html; charset=utf-8'})
	return URL.createObjectURL(blob)
}

!function(){
	var htm = `<html><head><meta name="viewport" content="width=device-width"></head><body><input onkeyup="_h1.innerText = this.value"><h1 id="_h1"></div></body></html>`
  var link = document.createElement('a');
	link.href = htmToObjURL(htm);
	link.innerText = 'Open the array URL';
	document.body.appendChild(link);
}()
