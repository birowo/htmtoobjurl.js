**#javascript**

### **convert html to object url blob**

**reference :** 
- https://developer.mozilla.org/en-US/docs/Web/API/Blob
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray/from

![htmToObjURL](https://user-images.githubusercontent.com/21541959/125012116-d6978280-e093-11eb-90c8-e3ea79318d82.png)
